#!/bin/bash

BASE_PATH=`dirname ${BASH_SOURCE[0]}`/..

npm i --prefix $BASE_PATH/client
npm i --prefix $BASE_PATH/server
