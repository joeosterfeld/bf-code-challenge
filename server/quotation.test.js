const { daysDifference, getAgeLoad, getQuoteTotal } = require('./quotation');

describe('daysDifference', () => {
    test('should get simple difference', () => {
        expect(daysDifference('2021-10-01', '2021-10-02')).toBe(2);
        expect(daysDifference('2021-01-01', '2021-01-10')).toBe(10);
        expect(daysDifference('2020-10-01', '2020-10-30')).toBe(30);
    });
    
    test('should difference across month end', () => {
        expect(daysDifference('2021-10-30', '2021-11-06')).toBe(8);
    });
    
    test('should difference across year end', () => {
        expect(daysDifference('2021-12-31', '2022-01-06')).toBe(7);
    });
});

it('should get proper age loads', () => {
    expect(getAgeLoad(18)).toBe(0.6);
    expect(getAgeLoad(25)).toBe(0.6);
    expect(getAgeLoad(30)).toBe(0.6);

    expect(getAgeLoad(31)).toBe(0.7);
    expect(getAgeLoad(35)).toBe(0.7);
    expect(getAgeLoad(40)).toBe(0.7);

    expect(getAgeLoad(41)).toBe(0.8);
    expect(getAgeLoad(45)).toBe(0.8);
    expect(getAgeLoad(50)).toBe(0.8);

    expect(getAgeLoad(51)).toBe(0.9);
    expect(getAgeLoad(55)).toBe(0.9);
    expect(getAgeLoad(60)).toBe(0.9);

    expect(getAgeLoad(61)).toBe(1);
    expect(getAgeLoad(65)).toBe(1);
    expect(getAgeLoad(70)).toBe(1);
});

fdescribe('getQuoteTotal', () => {

    test('calculates correctly per insruction example', () => {
        const total = getQuoteTotal({
            start_date: '2020-10-01',
            end_date: '2020-10-30',
            currency_id: 'EUR',
            age: '28,35'
        });
        expect(total).toBe(117);
    });

    test('calculates correctly per single traveler example', () => {
        const total = getQuoteTotal({
            start_date: '2020-10-01',
            end_date: '2020-10-30',
            currency_id: 'EUR',
            age: '28'
        });
        expect(total).toBe(54);
    });
});