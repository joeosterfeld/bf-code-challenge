
const fixedRate = 3;

/**
 * Note: I did this quickly instead of implementing a date library like
 * date-fns. I would import a lib if more date calculations were necessary.
 */
function daysDifference(startDate, endDate) {
    const oneDayMs = 1000 * 3600 * 24;
    const msDifference = new Date(endDate).getTime() - (new Date(startDate).getTime() - oneDayMs);
    const days = Math.ceil(msDifference / oneDayMs);
    return days;
}

/**
 * This switch case is an admittedly whacky way to do a comparison in js,
 * but seemed to be the least number of lines. I do not feel strongly about it,
 * and could see an if/else here instead.
 */
function getAgeLoad(age) {
    switch (true) {
        case age <= 30:
            return 0.6;
        case age <= 40:
            return 0.7;
        case age <= 50:
            return 0.8;
        case age <= 60:
            return 0.9;
        default:
            return 1;
    }
}

function getQuoteTotal(quoteRequestBody) {
    const numDays = daysDifference(quoteRequestBody.start_date, quoteRequestBody.end_date);
    const ageLoads = quoteRequestBody.age.split(',').map(loadStr => getAgeLoad(Number(loadStr)));
    const roundedString = ageLoads.reduce((accum, ageLoad) => accum + (fixedRate * ageLoad * numDays), 0).toFixed(2);
    return Number(roundedString);
}

module.exports = { daysDifference, getAgeLoad, getQuoteTotal };