const jwt = require('jsonwebtoken');

/**
 * Note: This key would never be committed to source code in a
 * production application. Leaving it in source here to remain within the
 * time contraints.
 */
const encryptionKey = 'battleface-is-exciting';

async function authorizeToken(name) {
    const expSeconds = 3600;
    return jwt.sign({
        data: { name },
        exp: new Date().getTime() + (expSeconds * 1000)
    }, encryptionKey);
}

async function verifyTokenMiddleware(req, res, next) {
    try {
        const authHeader = req.get('Authorization');
        const token = authHeader ? authHeader.split(' ')[1] : '';
        req.decodedToken = await jwt.verify(token, encryptionKey);
        next();
    } catch (err) {
        res.status(401).json({ status: 401, message: err.message });
    }
}

module.exports = { authorizeToken, verifyTokenMiddleware };
