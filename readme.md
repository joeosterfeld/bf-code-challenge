# Battleface Coding Challenge

## Prerequisites

- NodeJS ~v14
- NPM ~6.4.1

## Getting Started

- Install dependencies

    ```bash
    ./scripts/install.sh
    ```

- Build the front end app
  - ** Be sure this build step runs successfully. Without it, the static files will not be populated for the back end app to serve.

    ```bash
    npm run build --prefix client
    ```

- Run the back end app

    ```bash
    npm start --prefix server
    ```

- View/Test the back end app at `http://localhost:8080`

## Project Layout

### Front End Code

- Located in `./client`
- This is an Angular app, with many auto-generated files.
- The code is configured to build into the `server` directory, at `./server/static/`

### Back End Code

- Located in `./server`
- This is a simple NodeJS Express app.
- The back end serves the front end code, once it's built.
- Contains the business logic for JWT authorization and quote calculation.