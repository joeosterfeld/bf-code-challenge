export const environment = {
  production: true,
  apiBaseUrl: ''
  // ^ This can be left an empty string, since the static files come from the back end api
};
