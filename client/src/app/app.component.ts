import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from './services/api.service';

enum CurrencyId {
  USD = 'USD',
  EUR = 'EUR',
  GBP = 'GBP'
}

@Component({
  selector: 'bf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  datePattern = /(\d{4})-(\d{2})-(\d{2})/;
  agePattern = /^(0|[1-9][0-9]*)$/;

  dateErr = 'Please provide a valid date in ISO 8601 format: YYYY-MM-DD';
  ageErr = 'Please provide a valid age between the 18 and 70';

  currencyIds = [
    CurrencyId.EUR,
    CurrencyId.GBP,
    CurrencyId.USD
    // There are many more currency IDs. Providing a short list in the interest of time and lines of code.
  ];

  form = new FormGroup({
    start_date: new FormControl('', [Validators.required, Validators.pattern(this.datePattern)]),
    end_date: new FormControl('', [Validators.required, Validators.pattern(this.datePattern)]),
    currency_id: new FormControl(CurrencyId.EUR, [Validators.required]),
    age: new FormArray([
      this.createAgeControl()
    ])
  });

  quote: any;

  constructor(
    private apiSrv: ApiService
  ) {}

  async ngOnInit(): Promise<void> {
    const isSignedIn = await this.apiSrv.isSignedIn();
    if (!isSignedIn) {
      this.apiSrv.register();
    }
  }

  createAgeControl(value?: number): FormControl {
    return new FormControl(value ?? '', [
      Validators.required, Validators.pattern(this.agePattern), Validators.min(18), Validators.max(70)
    ]);
  }

  async submitForm(): Promise<void> {
    const formValueObj = this.form.getRawValue();
    const requestBody = { ...formValueObj, age: formValueObj.age.join() };
    this.quote = await this.apiSrv.getQuote(requestBody);
  }

  travelerInputId(index: number): string {
    return `traveler-${index + 1}-input`;
  }

  get ageFormArray(): FormArray {
    return (this.form.get('age') as FormArray);
  }
}
