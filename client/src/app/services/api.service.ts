import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly baseUrl = environment.apiBaseUrl;
  private readonly tokenLocalStorageKey = 'bf-token';

  constructor(
    private httpClient: HttpClient
  ) { }

  async register(): Promise<void> {
    /**
     * Note: This little prompt is primitive and has been implemented in place of
     * a username/password login or registration process. From interpereting the requirements,
     * I must protect the API via JSON web tokens, but it appears there's no requirement to
     * implement a username and password flow.
     */
    const name = window.prompt('Please enter your full name...');
    if (name) {
      const { token } = await this.httpClient.post(
        `${this.baseUrl}/authorize`, { name }, this.requestOptions).toPromise() as { token: string };
      this.token = token;
    } else {
      alert('Please enter a name and email to continue...');
      this.register();
    }
  }

  async isSignedIn(): Promise<boolean> {
    try {
      if (!this.token) {
        throw new Error('No token saved');
      }
      await this.httpClient.get(`${this.baseUrl}/verify`, this.requestOptions).toPromise();
      return true;
    } catch (err) {
      return false;
    }
  }

  getQuote(requestBody: object): Promise<object> {
    return this.httpClient.post(`${this.baseUrl}/quotation`, requestBody, this.requestOptions).toPromise();
  }

  private get token(): string {
    return localStorage.getItem(this.tokenLocalStorageKey) as string;
  }

  private set token(tokenValue) {
    localStorage.setItem(this.tokenLocalStorageKey, tokenValue);
  }

  /**
   * Note: In Angular, I would normally provide these headers automatically in
   * an http interceptor. Not creating one in the interest of time.
   */
  private get requestOptions(): object {
    return {
      headers: {
        Authorization: `Bearer ${this.token}`,
        'Content-Type': 'application/json'
      }
    };
  }
}
